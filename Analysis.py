import networkx as nx
from networkx.algorithms import components
from networkx.algorithms import cycles
import pandas as pd
import numpy as np

data = pd.read_csv('Graph2.csv', engine='python')
data = data.values
G = nx.Graph()
for row in data:
    G.add_edges_from([(row[0], row[1])])



with open('Stat_Report.txt', 'wb') as file:

    # Statistical Report
    file.write('number of nodes: {}\n'.format(len(G.nodes)))
    #print 'number of nodes:', len(G.nodes)
    file.write('number of edges: {}\n'.format(len(G.edges)))
    #print 'number of edges:', len(G.edges)
    tuple =  nx.degree(G)
    degrees = []
    for n in tuple:
        degrees.append(n[1])
    file.write('Degrees avg: {}\n'.format(np.mean(degrees)))
    #print 'degrees avg :', np.mean(degrees)
    file.write('min degree: {}\n'.format(min(degrees)))
    #print 'min degree :', min(degrees)
    file.write('max degree: {}\n'.format(max(degrees)))
    #print 'max degree :', max(degrees)
file.close()

with open('Components_Report.txt', 'wb') as file:

    # Extract Connected Components
    file.write('Number of Connected Components: {}\n'.format(components.number_connected_components(G)))
    #print 'Number of Connected Components:', components.number_connected_components(G)
    file.write('Connected Components: {}'.format(list(components.connected_components(G))))
    #print 'Connected Components', list(components.connected_components(G))
file.close()

with open('Cycles_Report.txt', 'wb') as file:
    # Extract Cycles
    file.write('Number of cycles: {}\n'.format(len(cycles.cycle_basis(G))))
    #print 'number of cycles :', len(cycles.cycle_basis(G))
    file.write('cycles: {}'.format(cycles.cycle_basis(G)))
    #print 'Cycles list :', cycles.cycle_basis(G)

file.close()


with open('Important_nodes_Report.txt', 'wb') as file:
    # Important nodes -------------------------------

    # Degree based
    tuple =  nx.degree(G)
    #print 'list important nodes based degree : ', sorted(tuple,key=lambda x: x[1], reverse= True)
    file.write('list important nodes ranking: {}'.format(sorted(tuple,key=lambda x: x[1], reverse= True)))

file.close()
